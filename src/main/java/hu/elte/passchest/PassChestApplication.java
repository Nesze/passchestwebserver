package hu.elte.passchest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PassChestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PassChestApplication.class, args);
    }

}

// PostgreSQL
// - Username: postgres
// - Password: firevillager