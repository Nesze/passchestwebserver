package hu.elte.passchest.service;

import hu.elte.passchest.model.entrydatabase.Entry;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;

public class MockEntryDatabaseService implements EntryDatabaseService {
	@Override
	public EntryDatabase getDb(String username, String password) {
		EntryDatabase entryDatabase = new EntryDatabase();
		if (username.equals("user1")) {
			Entry entry = new Entry();
			entry.setDisplayName("twitter");
			entryDatabase.getEntries().add(entry);
		}
		return entryDatabase;
	}
	
	@Override
	public boolean setDb(String username, String password, EntryDatabase entryDatabase) {
		return true;
	}
}
