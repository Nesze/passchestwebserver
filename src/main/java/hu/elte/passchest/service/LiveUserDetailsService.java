package hu.elte.passchest.service;

import hu.elte.passchest.model.CustomUserDetails;
import hu.elte.passchest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LiveUserDetailsService implements UserDetailsService {

    private final UserRepository repo;

    @Autowired
    public LiveUserDetailsService(UserRepository repo) {
        this.repo = repo;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return new CustomUserDetails(repo.findByUsername(s));
    }

}
