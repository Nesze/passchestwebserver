package hu.elte.passchest.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elte.passchest.config.SecurityConfiguration;
import hu.elte.passchest.model.dto.UserRequest;
import hu.elte.passchest.model.dto.UserValidation;
import hu.elte.passchest.model.entity.User;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import hu.elte.passchest.repository.UserRepository;
import hu.elte.passchest.security.EntryDatabaseEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.AEADBadTagException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Date;

@Service
public class LiveUserService implements UserService {

    private EntryDatabaseEncryptor encryptor;
    private ObjectMapper objectMapper;


    private UserRepository userRepository;

    @Autowired
    public LiveUserService(UserRepository userRepository) {
        this.userRepository = userRepository;

        this.encryptor = new EntryDatabaseEncryptor();
        this.objectMapper = new ObjectMapper();
    }

    private String encodePassword(String password) {
        return SecurityConfiguration.encoder().encode(password);
    }

    @Override
    public User getUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserValidation editEmail(String username, String newEmail) {
        UserValidation userStatus = new UserValidation();

        User user = userRepository.findByUsername(username);
        if (user != null) {
            user.setEmail(newEmail);
            userRepository.save(user);
        } else {
            userStatus.setUsernameStatus(UserValidation.Status.INVALID);
        }
        return userStatus;
    }

    @Override
    public UserValidation editPassword(String username, String oldPassword, String newPassword) {
        UserValidation userStatus = new UserValidation();

        User user = userRepository.findByUsername(username);
        if (user != null) {
            try {
                EntryDatabase userEntryDatabase = encryptor.decryptDb(user.getEntryDb(), oldPassword, user.getEntryDbSalt());

                user.setPasswordHash(encodePassword(newPassword));
                user.setEntryDbSalt(encryptor.getRandomSalt(8));
                user.setEntryDb(encryptor.encryptDb(userEntryDatabase, newPassword, user.getEntryDbSalt()));

                userRepository.save(user);
            } catch (InvalidKeyException | AEADBadTagException | IOException e) {
                e.printStackTrace();
            }
        } else {
            userStatus.setUsernameStatus(UserValidation.Status.INVALID);
        }
        return userStatus;
    }

    @Override
    public UserValidation registerUser(UserRequest newUser) {
        UserValidation userStatus = new UserValidation();

        User user = userRepository.findByUsername(newUser.getUsername());
        if (user == null) {
            try {
                user = new User(newUser.getEmail(), newUser.getUsername(), encodePassword(newUser.getPassword()));
                user.setEntryDbSalt(encryptor.getRandomSalt(8));

                byte[] encrypted = encryptor.encryptDb(new EntryDatabase(), newUser.getPassword(), user.getEntryDbSalt());
                user.setEntryDb(encrypted);
                user.setEntryDbDate(new Date());

                userRepository.save(user);
            } catch (JsonProcessingException | InvalidKeyException e) {
                e.printStackTrace();
            }
        } else {
            userStatus.setUsernameStatus(UserValidation.Status.OCCUPIED);
        }
        return userStatus;
    }

}
