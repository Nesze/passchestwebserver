package hu.elte.passchest.service;

import hu.elte.passchest.model.dto.UserRequest;
import hu.elte.passchest.model.dto.UserValidation;
import hu.elte.passchest.model.entity.User;

public interface UserService {

    User getUser(String username);

    UserValidation editEmail(String username, String newEmail);

    UserValidation editPassword(String username, String oldPassword, String newPassword);

    UserValidation registerUser(UserRequest newUser);

}
