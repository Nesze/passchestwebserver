package hu.elte.passchest.service;

import hu.elte.passchest.model.dto.UserRequest;
import hu.elte.passchest.model.dto.UserValidation;
import hu.elte.passchest.model.entity.User;

public class MockUserService implements UserService {
	@Override
	public User getUser(String username) {
		if (username.equals("user1")) {
			User user = new User();
			user.setUsername("user1");
			return user;
		} else {
			return null;
		}
	}
	
	@Override
	public UserValidation editEmail(String username, String newEmail) {
		return new UserValidation();
	}
	
	@Override
	public UserValidation editPassword(String username, String oldPassword, String newPassword) {
		return new UserValidation();
	}
	
	@Override
	public UserValidation registerUser(UserRequest newUser) {
		UserValidation response = new UserValidation();
		if (newUser.getUsername().equals("occupiedUser")) {
			response.setUsernameStatus(UserValidation.Status.OCCUPIED);
		}
		return response;
	}
}
