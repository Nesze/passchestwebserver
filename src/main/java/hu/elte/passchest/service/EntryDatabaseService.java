package hu.elte.passchest.service;

import hu.elte.passchest.model.entrydatabase.EntryDatabase;

public interface EntryDatabaseService {

    EntryDatabase getDb(String username, String password);

    boolean setDb(String username, String password, EntryDatabase entryDatabase);

}
