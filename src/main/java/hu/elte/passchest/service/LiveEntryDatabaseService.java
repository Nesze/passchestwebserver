package hu.elte.passchest.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import hu.elte.passchest.model.entity.User;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import hu.elte.passchest.repository.UserRepository;
import hu.elte.passchest.security.EntryDatabaseEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.AEADBadTagException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Date;

@Service
public class LiveEntryDatabaseService implements EntryDatabaseService {

    private final UserRepository userRepository;

    private final EntryDatabaseEncryptor encryptor;

    @Autowired
    public LiveEntryDatabaseService(UserRepository userRepository) {
        this.encryptor = new EntryDatabaseEncryptor();
        this.userRepository = userRepository;
    }

    @Override
    public EntryDatabase getDb(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            try{
                return encryptor.decryptDb(user.getEntryDb(), password, user.getEntryDbSalt());
            } catch (InvalidKeyException | AEADBadTagException | IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public boolean setDb(String username, String password, EntryDatabase entryDatabase) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            try {
                user.setEntryDbSalt(encryptor.getRandomSalt(8));
                byte[] encrypted = encryptor.encryptDb(entryDatabase, password, user.getEntryDbSalt());
                user.setEntryDb(encrypted);
                user.setEntryDbDate(new Date());
                userRepository.save(user);
                return true;
            } catch (InvalidKeyException | JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}
