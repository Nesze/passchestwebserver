package hu.elte.passchest.controller;

import hu.elte.passchest.model.dto.UserRequest;
import hu.elte.passchest.model.dto.UserDto;
import hu.elte.passchest.model.dto.UserValidation;
import hu.elte.passchest.model.entity.User;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import hu.elte.passchest.service.EntryDatabaseService;
import hu.elte.passchest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/v1")
public class APIController {

    private final UserService userService;
    private final EntryDatabaseService entryDatabaseService;

    @Autowired
    public APIController(UserService userService, EntryDatabaseService entryDatabaseService) {
        this.userService = userService;
        this.entryDatabaseService = entryDatabaseService;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/user",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public UserDto getUser(Principal principal) {
        User user = userService.getUser(principal.getName());
        return convertToUserDto(user);
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            value = "/user/register",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<?> registerUser(@RequestBody @Valid UserRequest userDetails, Errors errors) {
        if (errors.hasErrors()) {
            return new ResponseEntity<>(new UserValidation(errors), HttpStatus.BAD_REQUEST);
        }

        UserValidation status = userService.registerUser(userDetails);
        if (status.getEmailStatus() == UserValidation.Status.OK && status.getUsernameStatus() == UserValidation.Status.OK && status.getPasswordStatus() == UserValidation.Status.OK) {
            return new ResponseEntity<>(status, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(status, HttpStatus.CONFLICT);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/user/editemail",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<?> editEmail(@RequestBody @Valid UserRequest userDetails, Errors errors, Principal principal) {
        if (errors.hasFieldErrors("email")) {
            return new ResponseEntity<>(new UserValidation(errors), HttpStatus.BAD_REQUEST);
        }

        UserValidation status = userService.editEmail(principal.getName(), userDetails.getEmail());
        if (status.getEmailStatus() == UserValidation.Status.OK) {
            if (status.getUsernameStatus() == UserValidation.Status.OK) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            }
            return new ResponseEntity<>(status, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/user/editpassword",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<?> editPassword(@RequestBody @Valid UserRequest userDetails, Errors errors, Principal principal) {
        if (errors.hasFieldErrors("password")) {
            return new ResponseEntity<>(new UserValidation(errors), HttpStatus.BAD_REQUEST);
        }

        UserValidation status = userService.editPassword(principal.getName(), SecurityContextHolder.getContext().getAuthentication().getCredentials().toString(), userDetails.getPassword());
        if (status.getPasswordStatus() == UserValidation.Status.OK) {
            if (status.getUsernameStatus() == UserValidation.Status.OK) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            }
            return new ResponseEntity<>(status, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/entrydb",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<?> getEntryDatabase(Principal principal) {
        EntryDatabase entryDatabase = entryDatabaseService.getDb(principal.getName(), SecurityContextHolder.getContext().getAuthentication().getCredentials().toString());
        if (entryDatabase != null) {
            return new ResponseEntity<>(entryDatabase, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/entrydb/upload",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<?> setEntryDatabase(@RequestBody @Valid EntryDatabase entryDatabase, Errors errors, Principal principal) {
        if (errors.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        boolean successfullySet = entryDatabaseService.setDb(principal.getName(), SecurityContextHolder.getContext().getAuthentication().getCredentials().toString(), entryDatabase);
        if (successfullySet) {
            return new ResponseEntity<>(new UserValidation(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Entity - DTO converters

    private UserDto convertToUserDto(User user) {
        return new UserDto(user);
    }

}
