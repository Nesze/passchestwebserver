package hu.elte.passchest.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class EntryDatabaseEncryptor {

    private static final String ALGORITHM = "AES/GCM/NoPadding";

    private SecureRandom secureRandom;
    private ObjectMapper objectMapper;

    public EntryDatabaseEncryptor() {
        this.secureRandom = new SecureRandom();
        this.objectMapper = new ObjectMapper();
    }

    public byte[] encodePw(String message, String secret) {
        return DigestUtils.sha256(message + secret);
    }

    public String getRandomSalt(int lengthInBytes) {
        byte[] salt = new byte[lengthInBytes];
        secureRandom.nextBytes(salt);
        return new String(Base64.encodeBase64(salt));
    }

    public byte[] encryptDb(EntryDatabase entryDatabase, String password, String salt) throws JsonProcessingException, InvalidKeyException {
        byte[] key = encodePw(password, salt);
        String entryDbJson = objectMapper.writeValueAsString(entryDatabase);
        try {
            return encrypt(entryDbJson, key);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return null;
    }

    public EntryDatabase decryptDb(byte[] encryptedEntryDatabase, String password, String salt) throws InvalidKeyException, AEADBadTagException, IOException {
        byte[] key = encodePw(password, salt);
        try {
            String decrypted = decrypt(encryptedEntryDatabase, key);
            return objectMapper.readValue(decrypted, EntryDatabase.class);
        } catch (AEADBadTagException e) {
            throw e;
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Encryptors

    private byte[] encrypt(String message, byte[] key) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        byte[] iv = new byte[12];
        secureRandom.nextBytes(iv);
        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

        Cipher cipher = Cipher.getInstance(ALGORITHM);
        SecretKey secretKey = createKey(key);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

        byte[] cipherText = cipher.doFinal(message.getBytes());

        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + iv.length + cipherText.length);
        byteBuffer.putInt(iv.length);
        byteBuffer.put(iv);
        byteBuffer.put(cipherText);
        return byteBuffer.array();
    }

    private String decrypt(byte[] message, byte[] key) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        ByteBuffer byteBuffer = ByteBuffer.wrap(message);
        int ivLength = byteBuffer.getInt();
        if(ivLength < 12 || ivLength >= 16) {
            throw new IllegalArgumentException("invalid iv length: " + ivLength);
        }

        byte[] iv = new byte[ivLength];
        byteBuffer.get(iv);
        byte[] cipherText = new byte[byteBuffer.remaining()];
        byteBuffer.get(cipherText);

        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

        Cipher cipher = Cipher.getInstance(ALGORITHM);
        SecretKey secretKey = createKey(key);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);

        byte[] decryptedValue = cipher.doFinal(cipherText);

        return new String(decryptedValue);
    }

    // Utils

    private SecretKey createKey(byte[] key) {
        return new SecretKeySpec(key, "AES");
    }

}