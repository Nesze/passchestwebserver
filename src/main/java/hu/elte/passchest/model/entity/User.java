package hu.elte.passchest.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    public User(String email, String username, String passwordHash) {
        this.email = email;
        this.username = username;
        this.passwordHash = passwordHash;
    }

    @Id
    @GeneratedValue
    private Long id;

    private String email;
    private String username;
    private String passwordHash;

    private String entryDbSalt;
    @Temporal(TemporalType.DATE)
    private Date entryDbDate;
    private byte[] entryDb;

}
