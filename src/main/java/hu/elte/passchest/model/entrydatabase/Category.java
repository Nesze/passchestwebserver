package hu.elte.passchest.model.entrydatabase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Getter @Setter @NoArgsConstructor
public class Category {
    
    @NotNull
    @JsonProperty("uuid")
    private UUID uuid;
    
    @NotNull
    @JsonProperty("status")
    private Status status;

    @NotEmpty
    @JsonProperty("name")
    private String name;
    
    @NotNull
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @JsonProperty("dateModified")
    private Date dateModified;

}
