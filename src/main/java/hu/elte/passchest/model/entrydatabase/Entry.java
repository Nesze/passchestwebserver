package hu.elte.passchest.model.entrydatabase;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Getter @Setter @NoArgsConstructor
public class Entry {

    @NotNull
    @JsonProperty("uuid")
    private UUID uuid;
    
    @NotNull
    @JsonProperty("status")
    private Status status;

    @JsonProperty("categoryUuid")
    private UUID categoryUuid;

    @JsonProperty("displayName")
    private String displayName;

    @NotEmpty
    @JsonProperty("url")
    private String url;

    @JsonProperty("username")
    private String username;

    @NotEmpty
    @JsonProperty("password")
    private String password;

    @NotNull
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @JsonProperty("dateModified")
    private Date dateModified;

}
