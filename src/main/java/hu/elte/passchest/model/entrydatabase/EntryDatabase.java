package hu.elte.passchest.model.entrydatabase;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class EntryDatabase {

    public EntryDatabase() {
        categories = new ArrayList<>();
        entries = new ArrayList<>();
    }

    @NotNull
    @JsonProperty("categories")
    private List<Category> categories;
    @NotNull
    @JsonProperty("entries")
    private List<Entry> entries;

}
