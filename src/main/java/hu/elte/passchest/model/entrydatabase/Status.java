package hu.elte.passchest.model.entrydatabase;

public enum Status {ACTIVE, DELETED}
