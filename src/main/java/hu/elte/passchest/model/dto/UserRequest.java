package hu.elte.passchest.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter @Setter
public class UserRequest {

    @NotEmpty
    @Email(message = "invalid email")
    @JsonProperty("email")
    private String email;

    @Size(min = 3, max = 32, message = "invalid username length")
    @JsonProperty("username")
    private String username;

    @Size(min = 8, message = "invalid password length")
    @JsonProperty("password")
    private String password;

}
