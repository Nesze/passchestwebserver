package hu.elte.passchest.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import hu.elte.passchest.model.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter @NoArgsConstructor
public class UserDto {

    public UserDto(User user) {
        this.email = user.getEmail();
        this.username = user.getUsername();
        this.entryDbDate = user.getEntryDbDate();
    }

    @JsonProperty("email")
    private String email;
    @JsonProperty("username")
    private String username;

    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @JsonProperty("entryDbDate")
    private Date entryDbDate;

}
