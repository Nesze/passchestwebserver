package hu.elte.passchest.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.Errors;

@Getter @Setter
public class UserValidation {

    public UserValidation() {
        setEmailStatus(Status.OK);
        setUsernameStatus(Status.OK);
        setPasswordStatus(Status.OK);
    }

    public UserValidation(Errors errors) {
        if (errors.hasFieldErrors("email")) {
            setEmailStatus(Status.INVALID);
        } else {
            setEmailStatus(Status.OK);
        }
        if (errors.hasFieldErrors("username")) {
            setUsernameStatus(Status.INVALID);
        } else {
            setUsernameStatus(Status.OK);
        }
        if (errors.hasFieldErrors("password")) {
            setPasswordStatus(Status.INVALID);
        } else {
            setPasswordStatus(Status.OK);
        }
    }

    public enum Status { OK, INVALID, OCCUPIED }

    @JsonProperty("emailStatus")
    private Status emailStatus;

    @JsonProperty("usernameStatus")
    private Status usernameStatus;

    @JsonProperty("passwordStatus")
    private Status passwordStatus;

}
