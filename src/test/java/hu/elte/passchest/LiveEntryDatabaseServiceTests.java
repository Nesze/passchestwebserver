package hu.elte.passchest;

import hu.elte.passchest.model.dto.UserRequest;
import hu.elte.passchest.model.entrydatabase.Category;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import hu.elte.passchest.repository.UserRepository;
import hu.elte.passchest.service.EntryDatabaseService;
import hu.elte.passchest.service.LiveEntryDatabaseService;
import hu.elte.passchest.service.LiveUserService;
import hu.elte.passchest.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class LiveEntryDatabaseServiceTests {

    @Autowired
    private UserRepository userRepository;

    private UserService userService;
    private EntryDatabaseService entryDatabaseService;

    @Before
    public void before() {
        this.userService = new LiveUserService(userRepository);
        this.entryDatabaseService = new LiveEntryDatabaseService(userRepository);
    }

    private UserRequest createSampleUserRequest() {
        UserRequest user = new UserRequest();
        user.setEmail("user@email.com");
        user.setUsername("user");
        user.setPassword("password");
        return user;
    }

    @Test
    public void getDb() {
        UserRequest request = createSampleUserRequest();
        userService.registerUser(request);

        Assert.assertNotNull(entryDatabaseService.getDb(request.getUsername(), request.getPassword()));
    }

    @Test
    public void setDb() {
        UserRequest request = createSampleUserRequest();
        userService.registerUser(request);

        EntryDatabase entryDatabase = new EntryDatabase();
        Category category = new Category();
        category.setName("asdf");
        entryDatabase.getCategories().add(category);

        entryDatabaseService.setDb(request.getUsername(), request.getPassword(), entryDatabase);

        EntryDatabase resultEntryDatabase = entryDatabaseService.getDb(request.getUsername(), request.getPassword());

        Assert.assertEquals(entryDatabase.getCategories().size(), resultEntryDatabase.getCategories().size());
    }

}
