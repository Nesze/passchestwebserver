package hu.elte.passchest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import hu.elte.passchest.model.entrydatabase.Category;
import hu.elte.passchest.model.entrydatabase.Entry;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import hu.elte.passchest.model.entrydatabase.Status;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

public class EntryDatabaseTests {

    @Test
    public void CategorySerialization() {
        Timestamp lastUpdate = Timestamp.valueOf(String.format("%04d-%02d-%02d %02d:%02d:%02d", 2019, 4, 2, 14, 3, 32));
        UUID uuid = UUID.randomUUID();
        
        Category category = new Category();
        category.setUuid(uuid);
        category.setStatus(Status.ACTIVE);
        category.setName("Financial");
        category.setDateModified(lastUpdate);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResult;

        try {
            jsonResult = objectMapper.writeValueAsString(category);
            try {
                JsonNode node = objectMapper.readTree(jsonResult);
    
                Date date = new Date();
                date.setTime(category.getDateModified().getTime());
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                String formattedDate = formatter.format(date);

                Assert.assertEquals(category.getUuid(), UUID.fromString(node.get("uuid").asText()));
                Assert.assertEquals(category.getStatus(), Status.valueOf(node.get("status").asText()));
                Assert.assertEquals(category.getName(), node.get("name").asText());
                Assert.assertEquals(formattedDate, node.get("dateModified").asText());
            } catch (IOException e) {
                Assert.fail();
            }
        } catch (JsonProcessingException e) {
            Assert.fail();
        }
    }

    @Test
    public void CategoryDeserialization() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonInput = mapper.createObjectNode();

        UUID uuid = UUID.randomUUID();
        
        jsonInput.put("uuid", uuid.toString());
        jsonInput.put("status", Status.ACTIVE.toString());
        jsonInput.put("name", "Financial");
        jsonInput.put("dateModified", "2019-04-02T12:03:32.000Z");

        try {
            Category category = mapper.readValue(jsonInput.toString(), Category.class);
    
            Date date = new Date();
            date.setTime(category.getDateModified().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            String formattedDate = formatter.format(date);

            Assert.assertEquals(UUID.fromString(jsonInput.get("uuid").asText()), category.getUuid());
            Assert.assertEquals(Status.valueOf(jsonInput.get("status").asText()), category.getStatus());
            Assert.assertEquals(jsonInput.get("name").asText(), category.getName());
            Assert.assertEquals(jsonInput.get("dateModified").asText(), formattedDate);
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void EntrySerialization() {
        Timestamp lastUpdate = Timestamp.valueOf(String.format("%04d-%02d-%02d %02d:%02d:%02d", 2019, 4, 2, 14, 3, 32));
        UUID entryUuid = UUID.randomUUID();
        UUID categoryUuid = UUID.randomUUID();
        
        Entry entry = new Entry();
        entry.setUuid(entryUuid);
        entry.setStatus(Status.ACTIVE);
        entry.setCategoryUuid(categoryUuid);
        entry.setDisplayName("MyBank");
        entry.setUrl("https://www.mybank.com/");
        entry.setUsername("lilbill@email.com");
        entry.setPassword("Password007");
        entry.setDateModified(lastUpdate);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResult;

        try {
            jsonResult = objectMapper.writeValueAsString(entry);
            try {
                JsonNode node = objectMapper.readTree(jsonResult);

                Date date = new Date();
                date.setTime(entry.getDateModified().getTime());
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                String formattedDate = formatter.format(date);

                Assert.assertEquals(entry.getUuid(), UUID.fromString(node.get("uuid").asText()));
                Assert.assertEquals(entry.getStatus(), Status.valueOf(node.get("status").asText()));
                Assert.assertEquals(entry.getCategoryUuid(), UUID.fromString(node.get("categoryUuid").asText()));
                Assert.assertEquals(entry.getDisplayName(), node.get("displayName").asText());
                Assert.assertEquals(entry.getUrl(), node.get("url").asText());
                Assert.assertEquals(entry.getUsername(), node.get("username").asText());
                Assert.assertEquals(entry.getPassword(), node.get("password").asText());
                Assert.assertEquals(formattedDate, node.get("dateModified").asText());
            } catch (IOException e) {
                Assert.fail();
            }
        } catch (JsonProcessingException e) {
            Assert.fail();
        }
    }

    @Test
    public void EntryDeserialization() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonInput = mapper.createObjectNode();
        UUID entryUuid = UUID.randomUUID();
        UUID categoryUuid = UUID.randomUUID();
        
        jsonInput.put("uuid", entryUuid.toString());
        jsonInput.put("status", Status.ACTIVE.toString());
        jsonInput.put("categoryUuid", categoryUuid.toString());
        jsonInput.put("displayName", "MyBank");
        jsonInput.put("url", "https://www.mybank.com/");
        jsonInput.put("username", "lilbill@email.com");
        jsonInput.put("password", "Password007");
        jsonInput.put("dateModified", "2019-04-02T12:03:32.000Z");

        try {
            Entry entry = mapper.readValue(jsonInput.toString(), Entry.class);

            Date date = new Date();
            date.setTime(entry.getDateModified().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            String formattedDate = formatter.format(date);

            Assert.assertEquals(UUID.fromString(jsonInput.get("uuid").asText()), entry.getUuid());
            Assert.assertEquals(Status.valueOf(jsonInput.get("status").asText()), entry.getStatus());
            Assert.assertEquals(UUID.fromString(jsonInput.get("categoryUuid").asText()), entry.getCategoryUuid());
            Assert.assertEquals(jsonInput.get("displayName").asText(), entry.getDisplayName());
            Assert.assertEquals(jsonInput.get("url").asText(), entry.getUrl());
            Assert.assertEquals(jsonInput.get("username").asText(), entry.getUsername());
            Assert.assertEquals(jsonInput.get("password").asText(), entry.getPassword());
            Assert.assertEquals(jsonInput.get("dateModified").asText(), formattedDate);
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void EntryDatabaseSerialization() {
        EntryDatabase entryDatabase = new EntryDatabase();

        Timestamp lastUpdate1 = Timestamp.valueOf(String.format("%04d-%02d-%02d %02d:%02d:%02d", 2019, 4, 2, 14, 3, 32));
        Timestamp lastUpdate2 = Timestamp.valueOf(String.format("%04d-%02d-%02d %02d:%02d:%02d", 2019, 4, 2, 17, 1, 57));
    
        UUID entry1Uuid = UUID.randomUUID();
        UUID entry2Uuid = UUID.randomUUID();
        UUID category1Uuid = UUID.randomUUID();
        UUID category2Uuid = UUID.randomUUID();
        
        Entry entry1 = new Entry();
        entry1.setUuid(entry1Uuid);
        entry1.setStatus(Status.DELETED);
        entry1.setCategoryUuid(category1Uuid);
        entry1.setDisplayName("MyBank");
        entry1.setUrl("https://www.mybank.com/");
        entry1.setUsername("lilbill@email.com");
        entry1.setPassword("Password007");
        entry1.setDateModified(lastUpdate1);

        Entry entry2 = new Entry();
        entry2.setUuid(entry2Uuid);
        entry2.setStatus(Status.ACTIVE);
        entry2.setCategoryUuid(category2Uuid);
        entry2.setDisplayName("Twitter");
        entry2.setUrl("https://twitter.com/");
        entry2.setUsername("lilbill@email.com");
        entry2.setPassword("SecretPW42");
        entry2.setDateModified(lastUpdate2);

        Category category1 = new Category();
        category1.setUuid(category1Uuid);
        category1.setStatus(Status.ACTIVE);
        category1.setName("Financial");

        Category category2 = new Category();
        category2.setUuid(category2Uuid);
        category2.setStatus(Status.ACTIVE);
        category2.setName("Social media");

        entryDatabase.getCategories().add(category1);
        entryDatabase.getCategories().add(category2);
        entryDatabase.getEntries().add(entry1);
        entryDatabase.getEntries().add(entry2);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResult;

        try {
            jsonResult = objectMapper.writeValueAsString(entryDatabase);
            try {
                JsonNode node = objectMapper.readTree(jsonResult);

                ObjectMapper mapper = new ObjectMapper();
                ObjectReader categoryReader = mapper.readerFor(new TypeReference<List<Category>>(){});
                ObjectReader entryReader = mapper.readerFor(new TypeReference<List<Entry>>(){});
                List<Category> categoryList = categoryReader.readValue(node.get("categories"));
                List<Entry> entryList = entryReader.readValue(node.get("entries"));

                Assert.assertEquals(categoryList.get(0).getUuid(), entryDatabase.getCategories().get(0).getUuid());
                Assert.assertEquals(categoryList.get(1).getUuid(), entryDatabase.getCategories().get(1).getUuid());
                Assert.assertEquals(entryList.get(0).getUuid(), entryDatabase.getEntries().get(0).getUuid());
                Assert.assertEquals(entryList.get(1).getUuid(), entryDatabase.getEntries().get(1).getUuid());
            } catch (IOException e) {
                Assert.fail();
            }
        } catch (JsonProcessingException e) {
            Assert.fail();
        }
    }

    @Test
    public void EntryDatabaseDeserialization() {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode jsonInput = mapper.createObjectNode();
        
        UUID category1Uuid = UUID.randomUUID();
        UUID category2Uuid = UUID.randomUUID();

        ObjectNode jsonCategory1 = mapper.createObjectNode();
        jsonCategory1.put("uuid", category1Uuid.toString());
        jsonCategory1.put("status", Status.ACTIVE.toString());
        jsonCategory1.put("name", "Financial");

        ObjectNode jsonCategory2 = mapper.createObjectNode();
        jsonCategory2.put("uuid", category2Uuid.toString());
        jsonCategory2.put("status", Status.ACTIVE.toString());
        jsonCategory2.put("name", "Social media");

        ArrayNode jsonCategories = mapper.createArrayNode();
        jsonCategories.add(jsonCategory1);
        jsonCategories.add(jsonCategory2);

        UUID entry1Uuid = UUID.randomUUID();
        UUID entry2Uuid = UUID.randomUUID();
        
        ObjectNode jsonEntry1 = mapper.createObjectNode();
        jsonEntry1.put("uuid", entry1Uuid.toString());
        jsonEntry1.put("status", Status.DELETED.toString());
        jsonEntry1.put("categoryUuid", category1Uuid.toString());
        jsonEntry1.put("displayName", "MyBank");
        jsonEntry1.put("url", "https://www.mybank.com/");
        jsonEntry1.put("username", "lilbill@email.com");
        jsonEntry1.put("password", "Password007");
        jsonEntry1.put("dateModified", "2019-04-02T12:03:32.000Z");

        ObjectNode jsonEntry2 = mapper.createObjectNode();
        jsonEntry2.put("uuid", entry2Uuid.toString());
        jsonEntry2.put("status", Status.ACTIVE.toString());
        jsonEntry2.put("categoryUuid", category2Uuid.toString());
        jsonEntry2.put("displayName", "Twitter");
        jsonEntry2.put("url", "https://twitter.com/");
        jsonEntry2.put("username", "lilbill@email.com");
        jsonEntry2.put("password", "SecretPW42");
        jsonEntry2.put("dateModified", "2019-04-02T17:01:57.000Z");

        ArrayNode jsonEntries = mapper.createArrayNode();
        jsonEntries.add(jsonEntry1);
        jsonEntries.add(jsonEntry2);

        jsonInput.putArray("categories").addAll(jsonCategories);
        jsonInput.putArray("entries").addAll(jsonEntries);

        try {
            EntryDatabase entryDatabase = mapper.readValue(jsonInput.toString(), EntryDatabase.class);

            ObjectReader categoryReader = mapper.readerFor(new TypeReference<List<Category>>(){});
            ObjectReader entryReader = mapper.readerFor(new TypeReference<List<Entry>>(){});
            List<Category> categoryList = categoryReader.readValue(jsonInput.get("categories"));
            List<Entry> entryList = entryReader.readValue(jsonInput.get("entries"));

            Assert.assertEquals(categoryList.size(), entryDatabase.getCategories().size());
            Assert.assertEquals(UUID.fromString(jsonCategory1.get("uuid").asText()), entryDatabase.getCategories().get(0).getUuid());
            Assert.assertEquals(UUID.fromString(jsonCategory2.get("uuid").asText()), entryDatabase.getCategories().get(1).getUuid());
            Assert.assertEquals(entryList.size(), entryDatabase.getEntries().size());
            Assert.assertEquals(UUID.fromString(jsonEntry1.get("uuid").asText()), entryDatabase.getEntries().get(0).getUuid());
            Assert.assertEquals(UUID.fromString(jsonEntry2.get("uuid").asText()), entryDatabase.getEntries().get(1).getUuid());
        } catch (IOException e) {
            Assert.fail();
        }
    }

}
