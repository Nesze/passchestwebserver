package hu.elte.passchest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import hu.elte.passchest.controller.APIController;
import hu.elte.passchest.model.dto.UserRequest;
import hu.elte.passchest.model.entrydatabase.Entry;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import hu.elte.passchest.model.entrydatabase.Status;
import hu.elte.passchest.service.EntryDatabaseService;
import hu.elte.passchest.service.MockEntryDatabaseService;
import hu.elte.passchest.service.MockUserService;
import hu.elte.passchest.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(value = APIController.class, secure = false)
public class APIControllerTests {
	
	@TestConfiguration
	static class APIControllerTestContextConfiguration {
		
		@Bean
		public UserService userService() {
			return new MockUserService();
		}
		
		@Bean
		public EntryDatabaseService entryDatabaseService() {
			return new MockEntryDatabaseService();
		}
	}
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private UserService userService;
	@Autowired
	private EntryDatabaseService entryDatabaseService;
	
	private ObjectMapper mapper;
	private ObjectWriter writer;
	
	private Authentication authentication;
	
	@Before
	public void init() {
		mapper = new ObjectMapper();
		writer = mapper.writer().withDefaultPrettyPrinter();
		authentication = new Authentication() {
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() { return null; }
			@Override
			public Object getCredentials() { return "password"; }
			@Override
			public Object getDetails() { return null; }
			@Override
			public Object getPrincipal() { return null; }
			@Override
			public boolean isAuthenticated() { return false; }
			@Override
			public void setAuthenticated(boolean b) throws IllegalArgumentException { }
			@Override
			public String getName() { return null; }
		};
	}
	
	@Test
	public void UserDetails() throws Exception {
		mvc.perform(get("/api/v1/user")
				.principal(() -> "user1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.username", is("user1")));
	}
	
	@Test
	public void RegisterUser_validRequest() throws Exception {
		UserRequest request = new UserRequest();
		request.setEmail("user@email.com");
		request.setUsername("user");
		request.setPassword("password");
		
		String requestJson = writer.writeValueAsString(request);
		
		mvc.perform(put("/api/v1/user/register")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.usernameStatus", is("OK")));
	}
	
	@Test
	public void RegisterUser_occupiedUser() throws Exception {
		UserRequest request = new UserRequest();
		request.setEmail("user@email.com");
		request.setUsername("occupiedUser");
		request.setPassword("password");
		
		String requestJson = writer.writeValueAsString(request);
		
		mvc.perform(put("/api/v1/user/register")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isConflict())
				.andExpect(jsonPath("$.usernameStatus", is("OCCUPIED")));
	}
	
	@Test
	public void RegisterUser_invalidData() throws Exception {
		UserRequest request = new UserRequest();
		request.setEmail("useremail.com");
		request.setUsername("no");
		request.setPassword("pass");
		
		String requestJson = writer.writeValueAsString(request);
		
		mvc.perform(put("/api/v1/user/register")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.emailStatus", is("INVALID")))
				.andExpect(jsonPath("$.usernameStatus", is("INVALID")))
				.andExpect(jsonPath("$.passwordStatus", is("INVALID")));
	}
	
	@Test
	public void EditEmail_validRequest() throws Exception {
		UserRequest request = new UserRequest();
		request.setEmail("user@email.com");
		
		String requestJson = writer.writeValueAsString(request);
		
		mvc.perform(post("/api/v1/user/editemail")
				.principal(() -> "user1")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.emailStatus", is("OK")));
	}
	
	@Test
	public void EditEmail_invalidEmail() throws Exception {
		UserRequest request = new UserRequest();
		request.setEmail("useremail.com");
		
		String requestJson = writer.writeValueAsString(request);
		
		mvc.perform(post("/api/v1/user/editemail")
				.principal(() -> "user1")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.emailStatus", is("INVALID")));
	}
	
	@Test
	public void EditPassword_validRequest() throws Exception {
		UserRequest request = new UserRequest();
		request.setPassword("password");
		
		String requestJson = writer.writeValueAsString(request);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		mvc.perform(post("/api/v1/user/editpassword")
				.principal(() -> "user1")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.passwordStatus", is("OK")));
	}
	
	@Test
	public void EditPassword_invalidPassword() throws Exception {
		UserRequest request = new UserRequest();
		request.setPassword("pass");
		
		String requestJson = writer.writeValueAsString(request);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		mvc.perform(post("/api/v1/user/editpassword")
				.principal(() -> "user1")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.passwordStatus", is("INVALID")));
	}
	
	@Test
	public void EntryDbDownload() throws Exception {
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		mvc.perform(get("/api/v1/entrydb")
				.principal(() -> "user1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.entries[0].displayName", is("twitter")));
	}
	
	@Test
	public void EntryDbUpload_validRequest() throws Exception {
		EntryDatabase request = new EntryDatabase();
		Entry entry = new Entry();
		entry.setUuid(UUID.randomUUID());
		entry.setStatus(Status.ACTIVE);
		entry.setCategoryUuid(null);
		entry.setDisplayName("twitter");
		entry.setUsername("use");
		entry.setPassword("password");
		entry.setDateModified(new Date());
		request.getEntries().add(entry);
		
		String requestJson = writer.writeValueAsString(request);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		mvc.perform(post("/api/v1/entrydb/upload")
				.principal(() -> "user1")
				.content(requestJson)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.usernameStatus", is("OK")));
		
	}
	
	@Test
	public void EntryDbUpload_invalidRequest() throws Exception {
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		mvc.perform(post("/api/v1/entrydb/upload")
				.principal(() -> "user1")
				.content("")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
		
	}

}
