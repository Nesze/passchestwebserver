package hu.elte.passchest;

import com.fasterxml.jackson.core.JsonProcessingException;
import hu.elte.passchest.model.entrydatabase.EntryDatabase;
import hu.elte.passchest.security.EntryDatabaseEncryptor;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.AEADBadTagException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

public class EntryDatabaseEncryptorTests {

    private EntryDatabaseEncryptor encryptor;

    public EntryDatabaseEncryptorTests() {
        this.encryptor = new EntryDatabaseEncryptor();
    }

    @Test
    public void encodePw_Consistency() {
        String password = "Password01";
        String secret = encryptor.getRandomSalt(8);

        byte[] result1 = encryptor.encodePw(password, secret);
        byte[] result2 = encryptor.encodePw(password, secret);

        Assert.assertArrayEquals(result1, result2);
    }

    @Test
    public void encodePw_ResultLength() {
        String shortPassword = "ShortPw01";
        String secret1 = encryptor.getRandomSalt(8);

        String longPassword = "ThisIsAVeryVeryLongPasswordThatNoOneShouldBeAbleToGuessEver!!!74582345978";
        String secret2 = encryptor.getRandomSalt(8);

        byte[] result1 = encryptor.encodePw(shortPassword, secret1);
        byte[] result2 = encryptor.encodePw(longPassword, secret2);

        // Encoded input should always be 256 bits, or 32 bytes long
        Assert.assertEquals(32, result1.length);
        Assert.assertEquals(32, result2.length);
    }

    @Test
    public void randomSalt_Randomness() {
        int testLength = 100;

        List<String> salts = new ArrayList<>();
        for (int i = 0; i < testLength; i++) {
            String salt = encryptor.getRandomSalt(8);
            if (salts.contains(salt)) {
                Assert.fail();
            } else {
                salts.add(salt);
            }
        }
        Assert.assertEquals(testLength, salts.size());
    }

    @Test
    public void encryptDecrypt_Consistency() {
        EntryDatabase data = new EntryDatabase();
        String password = "password01";
        String salt = encryptor.getRandomSalt(8);

        byte[] cipherText;

        try {
            cipherText = encryptor.encryptDb(data, password, salt);
        } catch (InvalidKeyException e) {
            Assert.fail();
            cipherText = new byte[0];
        } catch (JsonProcessingException e) {
            Assert.fail();
            cipherText = new byte[0];
        }

        EntryDatabase decrypted = null;

        try {
            decrypted = encryptor.decryptDb(cipherText, password, salt);
        } catch (InvalidKeyException | AEADBadTagException e) {
            Assert.fail();
        } catch (IOException e) {
            Assert.fail();
        }

        Assert.assertEquals(data.getEntries().size(), decrypted.getEntries().size());
    }

    @Test
    public void encryptDecrypt_InvalidKey() {
        EntryDatabase data = new EntryDatabase();
        String password1 = "password01";
        String salt1 = encryptor.getRandomSalt(8);

        byte[] cipherText;

        try {
            cipherText = encryptor.encryptDb(data, password1, salt1);
        } catch (InvalidKeyException e) {
            Assert.fail();
            cipherText = new byte[0];
        } catch (JsonProcessingException e) {
            Assert.fail();
            cipherText = new byte[0];
        }

        String password2 = "password02";
        String salt2 = encryptor.getRandomSalt(8);
        EntryDatabase decrypted = null;

        try {
            decrypted = encryptor.decryptDb(cipherText, password2, salt2);
            Assert.fail();
        } catch (InvalidKeyException | AEADBadTagException e) {
            Assert.assertNull(decrypted);
        } catch (IOException e) {
            Assert.fail();
        }

    }

}
