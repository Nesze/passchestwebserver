package hu.elte.passchest;

import hu.elte.passchest.config.SecurityConfiguration;
import hu.elte.passchest.model.dto.UserRequest;
import hu.elte.passchest.model.dto.UserValidation;
import hu.elte.passchest.model.entity.User;
import hu.elte.passchest.repository.UserRepository;
import hu.elte.passchest.security.EntryDatabaseEncryptor;
import hu.elte.passchest.service.LiveUserService;
import hu.elte.passchest.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.AEADBadTagException;
import java.io.IOException;
import java.security.InvalidKeyException;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class LiveUserServiceTests {

    @Autowired
    private UserRepository userRepository;

    private UserService service;

    @Before
    public void before() {
        this.service = new LiveUserService(userRepository);
    }

    private UserRequest createSampleUserRequest() {
        UserRequest user = new UserRequest();
        user.setEmail("user@email.com");
        user.setUsername("user");
        user.setPassword("password");
        return user;
    }

    @Test
    public void registerUser_NewAndExisting() {
        UserRequest request = createSampleUserRequest();

        UserValidation validation1 = service.registerUser(request);
        Assert.assertEquals(UserValidation.Status.OK, validation1.getEmailStatus());
        Assert.assertEquals(UserValidation.Status.OK, validation1.getUsernameStatus());
        Assert.assertEquals(UserValidation.Status.OK, validation1.getPasswordStatus());

        UserValidation validation2 = service.registerUser(request);
        Assert.assertEquals(UserValidation.Status.OK, validation2.getEmailStatus());
        Assert.assertEquals(UserValidation.Status.OCCUPIED, validation2.getUsernameStatus());
        Assert.assertEquals(UserValidation.Status.OK, validation2.getPasswordStatus());
    }

    @Test
    public void getUser_ExistingAndNonexistent() {
        UserRequest request = createSampleUserRequest();
        service.registerUser(request);

        User response1 = service.getUser(request.getUsername());
        User response2 = service.getUser("nonExistentUser");

        Assert.assertNotNull(response1);
        Assert.assertNull(response2);
    }

    @Test
    public void editEmail() {
        UserRequest request = createSampleUserRequest();
        service.registerUser(request);

        User user = service.getUser(request.getUsername());
        Assert.assertEquals(request.getEmail(), user.getEmail());

        request.setEmail("edited@email.com");
        service.editEmail(request.getUsername(), request.getEmail());
        user = service.getUser(request.getUsername());
        Assert.assertEquals(request.getEmail(), user.getEmail());
    }

    @Test
    public void editPassword_PasswordAndEntryDbEncryptionChanges() {
        EntryDatabaseEncryptor encryptor = new EntryDatabaseEncryptor();
        UserRequest request = createSampleUserRequest();
        service.registerUser(request);

        User user = service.getUser(request.getUsername());
        Assert.assertTrue(SecurityConfiguration.encoder().matches(request.getPassword(), user.getPasswordHash()));
        try {
            Assert.assertNotNull(encryptor.decryptDb(user.getEntryDb(), request.getPassword(), user.getEntryDbSalt()));
        } catch (InvalidKeyException | AEADBadTagException | IOException e) {
            Assert.fail();
        }

        String newPassword = "newPassword";
        service.editPassword(request.getUsername(), request.getPassword(), newPassword);
        Assert.assertTrue(SecurityConfiguration.encoder().matches(newPassword, user.getPasswordHash()));
        try {
            Assert.assertNotNull(encryptor.decryptDb(user.getEntryDb(), newPassword, user.getEntryDbSalt()));
        } catch (InvalidKeyException | AEADBadTagException | IOException e) {
            Assert.fail();
        }
    }

}
